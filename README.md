# Chrome Extension for Chat Recommendation Interface for Zendesk

## Requirements
 - nodejs
 - npm

Tested on `v10.15.3` and `6.9.0` respectively.

Run `npm install` in the top level directory.

A websocket server must be available at `localhost:8127` that takes a string
message and sends a list of strings.

---

## Development

Transpile the Typescript using:
```{bash}
npm run tsc
```

Loading the extension in the browser:

 - navigate to `chrome://extensions`
 - turn `Developer mode` on
 - click `Load unpacked`
 - choose this project's top level directory