"use strict";
/**
 * content.js gets run on the zendesk chat page.
 *
 * The goal of this script is to inject a drop down menu for the customer
 * service agents. This drop down is populated contextually with suggestions
 * for their next utterances.
 *
 * The suggestions come from ws_server.py
 *
 * Ideas for adding the dropdown to the chat adapted from code from:
 * https://medium.com/@jh3y/how-to-where-s-the-caret-getting-the-xy-position-of-the-caret-a24ba372990a
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
(function () { return __awaiter(_this, void 0, void 0, function () {
    var sleep, assertNonNull, nullableAgentName, setAgentName, nullableAgentId, agentName, agentId, currentKey, HOST, PATH, PORT, SECURE, RECONNECT_ATTEMPTS, RECONNECT_INTERVAL_MS, nReconnectAttempts, queryParams, protocol, connectionString, suggestions, textArea, currentText, socket, connectWebSocket, styleSheet, TAB, UP_ARROW, DOWN_ARROW, LEFT_ARROW, RIGHT_ARROW, ENTER, SHIFT, currentCustomer, customerChats, dropDownShowing, dropDown, selectedItem, enterPressed, noTextAndSelection, zendeskShortcuts, cumulativeOffset, createDropdown, formatSuggestionText, putSelectedIntoTextArea, toggleItem, updateList, selectItem, setDropDownPosition, toggleDropdown, resetSuggestions, uuidv4, currentPartial, getNewSuggestions, zendeskSuppressed, handleZendeskKeyboardInput, shiftPressed, setShiftPressed, unsetShiftPressed, respondToKeyboard, showDropDown, getChatLog, manageNewUtterances, monitorChatLog, getCustomerName, clearTextAreaAndDropdown;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                // verify that the content script is running on the page
                console.log('AnswerIQ Chrome Extension for Chat is running');
                sleep = function (ms) {
                    return new Promise(function (resolve) { return setTimeout(resolve, ms); });
                };
                assertNonNull = function (o) {
                    if (o == null) {
                        console.log('ASSERTION ERROR: Non-Null Assertion Error');
                    }
                    return o;
                };
                nullableAgentName = null;
                setAgentName = function () {
                    var nameDiv = document.getElementsByClassName('username')[0];
                    if (nameDiv) {
                        nullableAgentName = nameDiv.textContent;
                    }
                };
                nullableAgentId = null;
                chrome.runtime.onMessage.addListener(function (backgroundMessage) {
                    if (backgroundMessage.agentId) {
                        nullableAgentId = backgroundMessage.agentId;
                    }
                });
                _a.label = 1;
            case 1:
                if (!!(nullableAgentName && nullableAgentId)) return [3 /*break*/, 3];
                if (!nullableAgentName) {
                    setAgentName();
                }
                return [4 /*yield*/, sleep(10)];
            case 2:
                _a.sent();
                return [3 /*break*/, 1];
            case 3:
                agentName = assertNonNull(nullableAgentName);
                agentId = assertNonNull(nullableAgentId);
                currentKey = '';
                HOST = 'aiq-gpu-test7.eastus.cloudapp.azure.com';
                PATH = '/';
                PORT = 8127;
                SECURE = true;
                RECONNECT_ATTEMPTS = 100;
                RECONNECT_INTERVAL_MS = 50;
                nReconnectAttempts = 0;
                queryParams = ('?agent_name=' + agentName + '&agent_id=' + agentId).split(' ').join('_');
                protocol = SECURE ? 'wss://' : 'ws://';
                connectionString = protocol + HOST + ':' + PORT + PATH + queryParams;
                suggestions = [];
                textArea = null;
                currentText = null;
                socket = null;
                connectWebSocket = function () {
                    return __awaiter(this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    socket = new WebSocket(connectionString);
                                    socket.onerror = function (err) {
                                        console.error('Socket encountered error: ', err.message, 'Closing socket');
                                        if (socket) {
                                            socket.close();
                                        }
                                    };
                                    socket.onclose = function () {
                                        return __awaiter(this, void 0, void 0, function () {
                                            return __generator(this, function (_a) {
                                                switch (_a.label) {
                                                    case 0:
                                                        nReconnectAttempts++;
                                                        if (!(nReconnectAttempts < RECONNECT_ATTEMPTS)) return [3 /*break*/, 2];
                                                        return [4 /*yield*/, sleep(RECONNECT_INTERVAL_MS)];
                                                    case 1:
                                                        _a.sent();
                                                        connectWebSocket();
                                                        _a.label = 2;
                                                    case 2: return [2 /*return*/];
                                                }
                                            });
                                        });
                                    };
                                    socket.onmessage = function (event) {
                                        var message = JSON.parse(event.data);
                                        console.log('received message', message);
                                        if (message.status == 'good') {
                                            console.log(message.key);
                                            console.log(currentKey);
                                            if (message.key == currentKey) {
                                                console.log('pushing suggestion');
                                                console.log(message.suggestion);
                                                suggestions.push(currentPartial + message.suggestion.replace('\n', '. '));
                                                if (dropDownShowing) {
                                                    updateList();
                                                }
                                                if (currentText && currentText.split('\n').slice(-1).pop() == '') {
                                                    toggleDropdown();
                                                }
                                            }
                                            else {
                                                // discard the message, it is from an old request
                                            }
                                        }
                                        else {
                                            // do nothing since the server has no messages 
                                        }
                                    };
                                    _a.label = 1;
                                case 1:
                                    if (!(socket.readyState !== WebSocket.OPEN)) return [3 /*break*/, 3];
                                    return [4 /*yield*/, sleep(RECONNECT_INTERVAL_MS)];
                                case 2:
                                    _a.sent();
                                    return [3 /*break*/, 1];
                                case 3:
                                    if (socket.readyState === WebSocket.OPEN) {
                                        nReconnectAttempts = 0;
                                    }
                                    return [2 /*return*/];
                            }
                        });
                    });
                };
                return [4 /*yield*/, connectWebSocket()];
            case 4:
                _a.sent();
                styleSheet = document.createElement('style');
                styleSheet.type = 'text/css';
                styleSheet.innerText = "\n  .input__marker {\n    background-color: #FFF;\n    border-radius: 4px;\n    display: block;\n    font-size: 13px;\n    padding: 4px 6px;\n    position: absolute;\n    transition: top 0.1s ease 0s, left 0.1s ease 0s;\n    white-space: nowrap;\n    width: auto;\n    z-index: 9999;\n  }\n  .input__marker:after {\n    background-color: #111;\n    content: '';\n    height: 10px;\n    position: absolute;\n    width: 15px;\n    z-index: -1;\n  }\n  .input__marker--position:after {\n    bottom: 0;\n    left: 0;\n    transform: translate(-10%, 10%) rotate(-15deg) skewX(-40deg);\n  }\n  .input__marker--selection {\n    transform: translate(-50%, -100%);\n  }\n  .input__marker--selection:after {\n    bottom: 0;\n    left: 50%;\n    transform: translate(-50%, 0) rotate(45deg);\n  }\n  .input__marker--custom:after {\n    display: none;\n  }\n  \n  .custom-suggestions {\n    cursor: pointer;\n    list-style: none;\n    margin: 0;\n    padding: 0 0 6px 0;\n    box-shadow: 0 2px 6px 0px rgba(0, 0, 0, 0.1);\n    border-bottom-left-radius: 6px;\n    border-bottom-right-radius: 6px;\n  }\n  .custom-suggestions--active {\n    background-color: #edf7ff;\n  }\n  .custom-suggestions li {\n    text-align: left;\n    text-overflow: ellipsis;\n    margin: 2px 0 0 2px;\n    padding: 2px 0 2px 2px;\n    border-bottom: 2px solid white;\n    white-space: nowrap;\n    overflow: hidden;\n  }\n  .custom-suggestions li:hover {\n    background-color: #edf7ff;\n  }\n";
                document.head.appendChild(styleSheet);
                TAB = 9;
                UP_ARROW = 38;
                DOWN_ARROW = 40;
                LEFT_ARROW = 37;
                RIGHT_ARROW = 39;
                ENTER = 13;
                SHIFT = 16;
                currentCustomer = null;
                customerChats = new Map();
                dropDownShowing = false;
                dropDown = null;
                selectedItem = null;
                enterPressed = false;
                noTextAndSelection = false;
                zendeskShortcuts = null;
                cumulativeOffset = function (element) {
                    var top = 0, left = 0;
                    do {
                        top += element.offsetTop || 0;
                        left += element.offsetLeft || 0;
                        element = element.offsetParent;
                    } while (element);
                    return {
                        x: left,
                        y: top
                    };
                };
                createDropdown = function () {
                    var marker = document.createElement('div');
                    marker.classList.add('input__marker', "input__marker--custom");
                    marker.textContent = null;
                    return marker;
                };
                formatSuggestionText = function (suggestion) {
                    var findCommonSubstring = function (tx1, tx2) {
                        var startIndex = tx1.toLowerCase().indexOf(tx2.toLowerCase());
                        var endIndex = startIndex + tx2.length;
                        if (startIndex == -1) {
                            startIndex = 0;
                            endIndex = 0;
                        }
                        return [startIndex, endIndex];
                    };
                    var text = assertNonNull(assertNonNull(textArea).value.split(/\r\n|\r|\n/).slice(-1).pop());
                    var _a = findCommonSubstring(suggestion, text), commonSubstringStart = _a[0], commonSubstringEnd = _a[1];
                    return '<strong>' + suggestion.slice(0, commonSubstringStart) + '</strong>' +
                        suggestion.slice(commonSubstringStart, commonSubstringEnd) +
                        '<strong>' + suggestion.slice(commonSubstringEnd) + '</strong>';
                };
                putSelectedIntoTextArea = function (selectedSuggestion) {
                    var getHightlights = function (textAreaText, selectedSuggestion) {
                        var savedText = assertNonNull(suggestions)[0];
                        var nNewLines = textAreaText.split('\n').length - 1;
                        if (noTextAndSelection) {
                            var start = textAreaText.length - selectedSuggestion.length - nNewLines;
                            return [start, textAreaText.length + nNewLines];
                        }
                        if (selectedSuggestion.startsWith(savedText)) {
                            var lines = textAreaText.split('\n');
                            if (lines.length == 1) {
                                return [savedText.length, textAreaText.length];
                            }
                            else {
                                return [lines.slice(0, -1).join('\n').length + savedText.length - (nNewLines - 1), textAreaText.length];
                            }
                        }
                        else {
                            return [textAreaText.length - selectedSuggestion.length - nNewLines, textAreaText.length];
                        }
                    };
                    if (currentText != null) {
                        var ta = assertNonNull(textArea);
                        var inputLines = currentText.split(/\r\n|\r|\n/);
                        var newTextValue = inputLines.slice(0, -1).join('\r\n');
                        if (newTextValue.length > 1) {
                            newTextValue += '\r\n';
                        }
                        newTextValue += selectedSuggestion;
                        if (enterPressed) {
                            newTextValue += '\r\n';
                        }
                        ta.value = newTextValue;
                        var _a = getHightlights(newTextValue, selectedSuggestion), highlightStart = _a[0], highlightEnd = _a[1];
                        if (highlightStart != highlightEnd) {
                            ta.setSelectionRange(highlightStart, highlightEnd);
                        }
                    }
                };
                toggleItem = function (direction) {
                    if (direction === void 0) { direction = 'next'; }
                    var newSelectedItem = null;
                    var dd = assertNonNull(dropDown);
                    var list = assertNonNull(dd.querySelector('ul'));
                    if (!selectedItem) {
                        newSelectedItem = dd.querySelector('li');
                    }
                    else {
                        selectedItem.classList.remove('custom-suggestions--active');
                        var nextActive = (direction === 'next' ? selectedItem.nextElementSibling : selectedItem.previousElementSibling);
                        if (!nextActive && direction === 'next') {
                            nextActive = list.firstChild;
                        }
                        else if (!nextActive) {
                            nextActive = list.lastChild;
                        }
                        newSelectedItem = nextActive;
                    }
                    newSelectedItem.classList.add('custom-suggestions--active');
                    selectedItem = newSelectedItem;
                    putSelectedIntoTextArea(assertNonNull(selectedItem.textContent));
                };
                updateList = function () {
                    // add in ability to know which option was previously being looked at
                    var dd = assertNonNull(dropDown);
                    var suggestedList = document.createElement('ul');
                    suggestedList.classList.add('custom-suggestions');
                    var boxWidth = document.getElementsByClassName('chat_actions_container')[0].clientWidth - 15;
                    var widthStyle = 'max-width:' + boxWidth + 'px; min-width:' + boxWidth + 'px;';
                    if (suggestions && textArea) {
                        suggestions.forEach(function (suggestion) {
                            var entryItem = document.createElement('li');
                            var formattedSuggestion = formatSuggestionText(suggestion);
                            entryItem.innerHTML = formattedSuggestion;
                            entryItem.setAttribute('style', widthStyle);
                            suggestedList.appendChild(entryItem);
                        });
                        if (dd.firstChild) {
                            dd.replaceChild(suggestedList, assertNonNull(dd.firstChild));
                        }
                        else {
                            dd.appendChild(suggestedList);
                        }
                        selectedItem = null;
                        if (currentText && currentText.split(/\r\n|\r|\n/).pop()) {
                            console.log('toggling item');
                            console.log(currentText);
                            toggleItem();
                        }
                    }
                };
                selectItem = function (suggestion) {
                    if (textArea) {
                        var currentValue = textArea.value.split(/\r\n|\r|\n/).slice(0, -1);
                        currentValue.push(suggestion);
                        textArea.value = currentValue.join('\r\n') + '\r\n';
                    }
                };
                setDropDownPosition = function (text) {
                    var dd = assertNonNull(dropDown);
                    var ta = assertNonNull(textArea);
                    var _a = cumulativeOffset(ta), x = _a.x, y = _a.y;
                    var left = x + 1;
                    var nLines = text.split(/\r\n|\r|\n/).length;
                    var top = y + 28 + ((Math.min(nLines, 8) - 1) * 16);
                    dd.setAttribute('style', "left: " + left + "px; top: " + top + "px");
                };
                toggleDropdown = function (ke) {
                    var clickItem = function (e) {
                        var ta = assertNonNull(textArea);
                        e.preventDefault();
                        var target = assertNonNull(e.target);
                        if (target.tagName === 'LI') {
                            ta.focus();
                            toggleDropdown(ke);
                            selectItem(assertNonNull(target.textContent));
                        }
                    };
                    var processClick = function (evt) {
                        if (ke !== evt && ke && evt.target !== ke.target) {
                            if (dropDownShowing) {
                                toggleDropdown();
                            }
                            else {
                                document.removeEventListener('click', processClick);
                            }
                        }
                    };
                    dropDownShowing = !dropDownShowing;
                    if (dropDownShowing && !dropDown) {
                        dropDown = createDropdown();
                        document.body.appendChild(dropDown);
                        dropDown.addEventListener('click', clickItem);
                        document.addEventListener('click', processClick);
                    }
                    else if (dropDown) {
                        dropDown.removeEventListener('click', clickItem);
                        document.body.removeChild(dropDown);
                        document.removeEventListener('click', processClick);
                        dropDown = null;
                        selectedItem = null;
                    }
                    if (dropDownShowing) {
                        setDropDownPosition(assertNonNull(textArea).value);
                    }
                };
                resetSuggestions = function () {
                    suggestions = [];
                    if (currentText) {
                        var lastLine = assertNonNull(currentText.split(/\r\n|\r|\n/).slice(-1).pop());
                        if (lastLine.length > 0) {
                            suggestions.push(lastLine);
                        }
                    }
                };
                uuidv4 = function () {
                    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                        return v.toString(16);
                    });
                };
                currentPartial = '';
                getNewSuggestions = function (messageType, customerName, text) {
                    currentKey = uuidv4();
                    var payload = null;
                    if (messageType == 'partial') {
                        currentPartial = assertNonNull(text.split('\n').slice(-1).pop());
                        payload = {
                            key: currentKey,
                            user_name: customerName,
                            partial: text
                        };
                    }
                    else if (messageType == 'user') {
                        payload = {
                            key: currentKey,
                            user_name: customerName,
                            user_utterance: text
                        };
                    }
                    else if (messageType = 'agent') {
                        payload = {
                            key: currentKey,
                            user_name: customerName,
                            agent_utterance: text
                        };
                    }
                    else {
                        console.log('WARNING: Unkown message type attempted to be sent to the server');
                        return;
                    }
                    if (socket && socket.readyState == WebSocket.OPEN && payload) {
                        resetSuggestions();
                        socket.send(JSON.stringify(payload));
                        // // wait for the suggestions to come in from the web socket
                        // // have a maximum number of time to wait
                        // for (let i = 0; i < 1000 && suggestions == null; i += 1) {
                        //     // waiting for the ws server to send back the suggestions
                        //     await sleep(10);
                        //     if (i == 1000 - 1) {
                        //         console.log('WARNING: took too long to get the next set of suggestions');
                        //     }
                        // }
                    }
                };
                zendeskSuppressed = function (text) {
                    if (text.length > 0 && text[0] == '/') {
                        return false;
                    }
                    return true;
                };
                handleZendeskKeyboardInput = function (e) {
                    var ta = assertNonNull(textArea);
                    var text = ta.value;
                    if (e.which == TAB) {
                        e.preventDefault();
                    }
                    else if ((e.which == UP_ARROW || e.which == DOWN_ARROW) && zendeskSuppressed(text)) {
                        // if (ta.selectionEnd == text.length) {
                        e.preventDefault();
                        // }
                    }
                };
                shiftPressed = false;
                setShiftPressed = function () {
                    shiftPressed = true;
                };
                unsetShiftPressed = function () {
                    shiftPressed = false;
                };
                respondToKeyboard = function (e) {
                    // which is the keyboard value being processed
                    var which = e.which;
                    enterPressed = which == ENTER && shiftPressed;
                    // const sideArrowPressed = which == LEFT_ARROW || which == RIGHT_ARROW
                    switch (which) {
                        case UP_ARROW:
                        case DOWN_ARROW:
                            if (assertNonNull(currentText).split(/\r\n|\r|\n/).slice(-1).pop() == '') {
                                noTextAndSelection = true;
                            }
                            toggleItem(which === UP_ARROW ? 'previous' : 'next');
                            break;
                        case SHIFT:
                            break;
                        default:
                            noTextAndSelection = false;
                            if (!enterPressed) {
                                updateList();
                            }
                    }
                    enterPressed = false;
                };
                showDropDown = function (e) {
                    return __awaiter(this, void 0, void 0, function () {
                        var ta, text, which, zendeskShortcutsArr, shouldSend, numSuggestions, offButShouldBeOn, onButShouldBeOff, toggle;
                        return __generator(this, function (_a) {
                            ta = assertNonNull(textArea);
                            text = ta.value;
                            which = e.which;
                            if (zendeskSuppressed(text)) {
                                zendeskShortcutsArr = document.getElementsByClassName('jx_ui_Widget textfieldlist_list_container');
                                if (zendeskShortcutsArr.length > 0) {
                                    zendeskShortcuts = zendeskShortcutsArr[0];
                                }
                                else {
                                    zendeskShortcuts = null;
                                }
                                if (![UP_ARROW, DOWN_ARROW, SHIFT].includes(which) && currentCustomer) {
                                    currentText = text;
                                    console.log('currentText', currentText);
                                    shouldSend = [
                                        '\r\n',
                                        '\r',
                                        '\n',
                                        ' ',
                                    ].some(function (elm) { return text.endsWith(elm); });
                                    suggestions[0] = assertNonNull(currentText.split('\n').slice(-1).pop());
                                    if (shouldSend) {
                                        console.log('sending partial');
                                        getNewSuggestions('partial', currentCustomer, text);
                                    }
                                }
                                if (suggestions) {
                                    if (which == ENTER && !shiftPressed) {
                                        toggleDropdown(e);
                                    }
                                    if (ta.selectionEnd != ta.value.length) {
                                        if (dropDownShowing) {
                                            toggleDropdown();
                                        }
                                        return [2 /*return*/];
                                    }
                                    numSuggestions = suggestions.length;
                                    offButShouldBeOn = !dropDownShowing && numSuggestions > 0;
                                    onButShouldBeOff = dropDownShowing && numSuggestions == 0;
                                    if (offButShouldBeOn && onButShouldBeOff) {
                                        console.log('ASSERTION ERROR: offButShouldBeOn and onButShouldBeOff should not both be true');
                                    }
                                    toggle = offButShouldBeOn || onButShouldBeOff;
                                    if (toggle) {
                                        toggleDropdown(e);
                                    }
                                    if (numSuggestions > 0) {
                                        setDropDownPosition(ta.value);
                                        respondToKeyboard(e);
                                    }
                                }
                                else if (dropDownShowing) {
                                    toggleDropdown(e);
                                }
                            }
                            else {
                                zendeskShortcuts = null;
                                if (dropDownShowing) {
                                    toggleDropdown(e);
                                }
                            }
                            return [2 /*return*/];
                        });
                    });
                };
                getChatLog = function () {
                    var chatDivs = Array.from(document.getElementsByClassName('chat_log_line'))
                        .filter(function (div) {
                        var divClasses = div.classList;
                        var containsSystem = divClasses.contains('system');
                        var containsUnverified = divClasses.contains('unverified');
                        return !(containsSystem || containsUnverified);
                    });
                    var previousName = '';
                    var chatLog = [];
                    chatDivs.forEach(function (chatDiv) {
                        // get the name if it is there and update the previous name
                        var headerContainer = Array.from(chatDiv.getElementsByClassName('header_container')).pop();
                        if (headerContainer) {
                            var nameDiv = Array.from(headerContainer.getElementsByClassName('name')).pop();
                            if (nameDiv) {
                                var name_1 = nameDiv.textContent;
                                if (name_1 && name_1 != '') {
                                    previousName = name_1;
                                }
                            }
                        }
                        var messageContainer = Array.from(chatDiv.getElementsByClassName('message_container')).pop();
                        if (messageContainer) {
                            var messageDiv = Array.from(messageContainer.getElementsByClassName('message'))
                                .filter(function (div) { return !div.classList.contains('translated'); })
                                .pop();
                            if (messageDiv) {
                                chatLog.push({
                                    name: previousName,
                                    message: messageDiv.textContent
                                });
                            }
                        }
                    });
                    return chatLog;
                };
                manageNewUtterances = function (newUtterances, customer) {
                    return __awaiter(this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            // the number of new of new utterances should not typically be more than one
                            if (newUtterances.length == 0) {
                                console.log('WARNING: length of the new utterances should be greater than 0');
                                return [2 /*return*/];
                            }
                            if (newUtterances.length > 1) {
                                console.log('WARNING: length of the new utterances should not typically be greater than 1. ' +
                                    'This means that the chat log updated faster than we are monitoring it. ' +
                                    'Also possible at beginning of chat.');
                            }
                            // send off the new utterances
                            newUtterances.forEach(function (chatMessage) {
                                var name = chatMessage.name;
                                var message = chatMessage.message;
                                var messageType = name == agentName ? 'agent' : 'user';
                                getNewSuggestions(messageType, customer, message);
                                currentPartial = '';
                            });
                            if (currentCustomer && newUtterances.pop().name == currentCustomer && suggestions && suggestions.length > 0) {
                                if (dropDownShowing) {
                                    toggleDropdown();
                                    toggleDropdown();
                                }
                                else {
                                    toggleDropdown();
                                }
                                updateList();
                            }
                            return [2 /*return*/];
                        });
                    });
                };
                monitorChatLog = function () {
                    if (currentCustomer) {
                        var currentChatLog = customerChats.get(currentCustomer);
                        if (currentChatLog) {
                            var newChatLog = getChatLog();
                            var nNewChats = newChatLog.length - currentChatLog.length;
                            if (nNewChats > 0) {
                                customerChats.set(currentCustomer, newChatLog);
                                manageNewUtterances(newChatLog.slice(-1 * nNewChats), currentCustomer);
                            }
                        }
                    }
                };
                getCustomerName = function () {
                    var customerNameElement = Array.from(document.getElementsByClassName('h3 visitor_name')).pop();
                    if (customerNameElement && customerNameElement.textContent) {
                        return customerNameElement.textContent;
                    }
                    console.log('ASSERTION ERROR: The customer name should be available when this is called');
                    return null;
                };
                clearTextAreaAndDropdown = function () {
                    if (textArea) {
                        textArea.removeEventListener('keydown', handleZendeskKeyboardInput);
                        textArea.removeEventListener('keypress', handleZendeskKeyboardInput);
                        textArea.removeEventListener('keyup', handleZendeskKeyboardInput);
                        textArea.removeEventListener('keyup', showDropDown);
                        textArea.removeEventListener('keydown', setShiftPressed);
                        textArea.removeEventListener('keyup', unsetShiftPressed);
                    }
                    if (dropDownShowing) {
                        toggleDropdown();
                    }
                    textArea = null;
                    // suggestions = null;
                    currentCustomer = null;
                };
                setInterval(function () {
                    var textAreaArr = Array.from(document.getElementsByClassName('meshim_dashboard_components_chatPanel_ChatTextAreaList chat_input'));
                    var chatWindow = Array.from(document.getElementsByClassName('meshim_dashboard_components_ChatPanel chat_panel')).pop();
                    if (chatWindow) {
                        if (chatWindow.classList.contains('active')) {
                            if (textAreaArr.length > 0) {
                                var cancelAndEndButtonsOff = false;
                                var cancelDiv = Array.from(document.getElementsByClassName('jx_ui_Widget container'))
                                    .filter(function (div) { return div.innerHTML.includes('EndChat'); }).pop();
                                if (cancelDiv) {
                                    var cancelStyle = cancelDiv.getAttribute('style');
                                    if (cancelStyle) {
                                        cancelAndEndButtonsOff = cancelStyle.includes('none');
                                    }
                                }
                                else {
                                    cancelAndEndButtonsOff = true;
                                }
                                if (!textArea) {
                                    if (cancelAndEndButtonsOff) {
                                        textArea = textAreaArr[0];
                                        textArea.addEventListener('keydown', handleZendeskKeyboardInput);
                                        textArea.addEventListener('keypress', handleZendeskKeyboardInput);
                                        textArea.addEventListener('keyup', handleZendeskKeyboardInput);
                                        textArea.addEventListener('keyup', showDropDown);
                                        textArea.addEventListener('keydown', setShiftPressed);
                                        textArea.addEventListener('keyup', unsetShiftPressed);
                                        currentCustomer = getCustomerName();
                                        if (currentCustomer && !customerChats.has(currentCustomer)) {
                                            customerChats.set(currentCustomer, []);
                                        }
                                    }
                                }
                                else if (!cancelAndEndButtonsOff) {
                                    clearTextAreaAndDropdown();
                                }
                            }
                        }
                        else {
                            clearTextAreaAndDropdown();
                        }
                    }
                }, 200);
                setInterval(function () {
                    if (zendeskShortcuts) {
                        zendeskShortcuts.setAttribute('style', 'display: none');
                    }
                }, 1);
                setInterval(monitorChatLog, 50);
                window.onresize = function () {
                    if (dropDownShowing) {
                        setDropDownPosition(assertNonNull(textArea).value);
                        updateList();
                    }
                };
                return [2 /*return*/];
        }
    });
}); })(); // end of wrapping entire script in async
