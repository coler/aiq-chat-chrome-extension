/**
 * content.js gets run on the zendesk chat page.
 * 
 * The goal of this script is to inject a drop down menu for the customer
 * service agents. This drop down is populated contextually with suggestions
 * for their next utterances.
 * 
 * The suggestions come from ws_server.py
 * 
 * Ideas for adding the dropdown to the chat adapted from code from:
 * https://medium.com/@jh3y/how-to-where-s-the-caret-getting-the-xy-position-of-the-caret-a24ba372990a
 */

(async () => {  // wrap entire script in async in order to wait for the synchronous events

// verify that the content script is running on the page
console.log('AnswerIQ Chrome Extension for Chat is running');


// helper functions
const sleep = function(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
};
const assertNonNull = function<T>(o: T | null) {
    if(o == null) {
        console.log('ASSERTION ERROR: Non-Null Assertion Error');
    }
    return o!;
};

let nullableAgentName: string | null = null;
const setAgentName = function() {
    const nameDiv = document.getElementsByClassName('username')[0];
    if (nameDiv) {
        nullableAgentName = nameDiv.textContent;
    }
}
let nullableAgentId: string | null = null;
chrome.runtime.onMessage.addListener(function(backgroundMessage: any) {
    if (backgroundMessage.agentId) {
        nullableAgentId = backgroundMessage.agentId;
    }
});

while (!(nullableAgentName && nullableAgentId)) {
    if (!nullableAgentName) {
        setAgentName()
    }
    await sleep(10);
}

const agentName = assertNonNull(nullableAgentName);
const agentId = assertNonNull(nullableAgentId);


let currentKey: string = '';


const HOST = 'aiq-gpu-test7.eastus.cloudapp.azure.com';
// const HOST = 'localhost';
const PATH = '/';
const PORT = 8127;
const SECURE = true;

const RECONNECT_ATTEMPTS = 100;
const RECONNECT_INTERVAL_MS = 50;
let nReconnectAttempts = 0;

const queryParams = ('?agent_name=' + agentName + '&agent_id=' + agentId).split(' ').join('_');
const protocol = SECURE ? 'wss://' : 'ws://'
const connectionString = protocol + HOST + ':' + PORT + PATH + queryParams;

// suggestions is the query result from the server
let suggestions: string[] = [];
// text area where the agent types in the chat
let textArea: HTMLTextAreaElement | null = null;

let currentText: string | null = null;

let socket: WebSocket | null = null;
const connectWebSocket = async function() {
    socket = new WebSocket(connectionString);

    socket.onerror = function(err: any) {
        console.error('Socket encountered error: ', err.message, 'Closing socket');
        if (socket) {
            socket.close();
        }
    };
    socket.onclose = async function() {
        nReconnectAttempts++;
        if (nReconnectAttempts < RECONNECT_ATTEMPTS) {
            await sleep(RECONNECT_INTERVAL_MS);
            connectWebSocket();
        }
    };
    socket.onmessage = function(event) {
        const message = JSON.parse(event.data);
        console.log('received message', message);
        if (message.status == 'good') {
            console.log(message.key);
            console.log(currentKey);
            if (message.key == currentKey) {
                console.log('pushing suggestion');
                console.log(message.suggestion);
                suggestions.push(currentPartial + message.suggestion.replace('\n', '. '));
                if (dropDownShowing) {
                    updateList();
                }
                if (currentText && currentText.split('\n').slice(-1).pop() == '') {
                    toggleDropdown();
                }
            }
            else {
                // discard the message, it is from an old request
            }
        }
        else {
            // do nothing since the server has no messages 
        }
    };

    while (socket.readyState !== WebSocket.OPEN) {
        await sleep(RECONNECT_INTERVAL_MS);
    }
    if (socket.readyState === WebSocket.OPEN) {
        nReconnectAttempts = 0;
    }
};
await connectWebSocket();

// add a custom styling to the page for the dropdown we are adding
let styleSheet = document.createElement('style');
styleSheet.type = 'text/css';
styleSheet.innerText = `
  .input__marker {
    background-color: #FFF;
    border-radius: 4px;
    display: block;
    font-size: 13px;
    padding: 4px 6px;
    position: absolute;
    transition: top 0.1s ease 0s, left 0.1s ease 0s;
    white-space: nowrap;
    width: auto;
    z-index: 9999;
  }
  .input__marker:after {
    background-color: #111;
    content: '';
    height: 10px;
    position: absolute;
    width: 15px;
    z-index: -1;
  }
  .input__marker--position:after {
    bottom: 0;
    left: 0;
    transform: translate(-10%, 10%) rotate(-15deg) skewX(-40deg);
  }
  .input__marker--selection {
    transform: translate(-50%, -100%);
  }
  .input__marker--selection:after {
    bottom: 0;
    left: 50%;
    transform: translate(-50%, 0) rotate(45deg);
  }
  .input__marker--custom:after {
    display: none;
  }
  
  .custom-suggestions {
    cursor: pointer;
    list-style: none;
    margin: 0;
    padding: 0 0 6px 0;
    box-shadow: 0 2px 6px 0px rgba(0, 0, 0, 0.1);
    border-bottom-left-radius: 6px;
    border-bottom-right-radius: 6px;
  }
  .custom-suggestions--active {
    background-color: #edf7ff;
  }
  .custom-suggestions li {
    text-align: left;
    text-overflow: ellipsis;
    margin: 2px 0 0 2px;
    padding: 2px 0 2px 2px;
    border-bottom: 2px solid white;
    white-space: nowrap;
    overflow: hidden;
  }
  .custom-suggestions li:hover {
    background-color: #edf7ff;
  }
`;
document.head.appendChild(styleSheet);

const TAB = 9;
const UP_ARROW = 38;
const DOWN_ARROW = 40;
const LEFT_ARROW = 37;
const RIGHT_ARROW = 39;
const ENTER = 13;
const SHIFT = 16;

let currentCustomer: string | null = null;
let customerChats: Map<string, any[]> = new Map();

let dropDownShowing = false;
let dropDown: HTMLElement | null = null;
let selectedItem: HTMLLIElement | null = null;
let enterPressed: boolean = false;

let noTextAndSelection = false;

let zendeskShortcuts: HTMLElement | null = null;

// showDropDown helper functions
// https://stackoverflow.com/a/1480137
const cumulativeOffset = function(element: HTMLElement) {
    let top = 0, left = 0;
    do {
        top += element.offsetTop  || 0;
        left += element.offsetLeft || 0;
        element = <HTMLElement> element.offsetParent;
    } while(element);

    return {
        x: left,
        y: top
    };
};

// create a marker for the input
const createDropdown = function() {
    const marker = document.createElement('div');
    marker.classList.add('input__marker', `input__marker--custom`);
    marker.textContent = null;
    return marker;
};

const formatSuggestionText = function(suggestion: string) {
    const findCommonSubstring = function(tx1: string, tx2: string) {
        let startIndex = tx1.toLowerCase().indexOf(tx2.toLowerCase());
        let endIndex = startIndex + tx2.length;
        if (startIndex == -1) {
            startIndex = 0;
            endIndex = 0;
        }
        return [startIndex, endIndex];
    }

    const text = assertNonNull(assertNonNull(textArea).value.split(/\r\n|\r|\n/).slice(-1).pop());

    const [ commonSubstringStart, commonSubstringEnd ] = findCommonSubstring(suggestion, text);

    return '<strong>' + suggestion.slice(0, commonSubstringStart) + '</strong>' + 
        suggestion.slice(commonSubstringStart, commonSubstringEnd) + 
        '<strong>' + suggestion.slice(commonSubstringEnd) + '</strong>';
};

const putSelectedIntoTextArea = function(selectedSuggestion: string) {
    const getHightlights = function(textAreaText: string, selectedSuggestion: string): number[] {
        const savedText = assertNonNull(suggestions)[0];
        const nNewLines = textAreaText.split('\n').length - 1;

        if (noTextAndSelection) {
            const start = textAreaText.length - selectedSuggestion.length - nNewLines;
            return [ start, textAreaText.length + nNewLines];
        }
        if (selectedSuggestion.startsWith(savedText)) {
            const lines = textAreaText.split('\n');
            if (lines.length == 1) {
                return [ savedText.length, textAreaText.length ];
            }
            else {
                return [ lines.slice(0, -1).join('\n').length + savedText.length - (nNewLines - 1), textAreaText.length ];
            }
        }
        else {
            return [ textAreaText.length - selectedSuggestion.length - nNewLines, textAreaText.length ];
        }
    }

    if (currentText != null) {
        const ta = assertNonNull(textArea);
        const inputLines = currentText.split(/\r\n|\r|\n/);
        let newTextValue = inputLines.slice(0, -1).join('\r\n');
        if (newTextValue.length > 1) {
            newTextValue += '\r\n';
        }
        newTextValue += selectedSuggestion;
        if (enterPressed) {
            newTextValue += '\r\n';
        }
        ta.value = newTextValue;
        const [ highlightStart, highlightEnd ] = getHightlights(newTextValue, selectedSuggestion);
        if (highlightStart != highlightEnd) {
            ta.setSelectionRange(highlightStart, highlightEnd);
        }
    }
}

const toggleItem = (direction: string = 'next') => {
    let newSelectedItem = null;
    const dd = assertNonNull(dropDown);
    const list = assertNonNull(dd.querySelector('ul'));
    if (!selectedItem) {
        newSelectedItem = <HTMLLIElement> dd.querySelector('li');
    }
    else {
        selectedItem.classList.remove('custom-suggestions--active');
        let nextActive = <HTMLLIElement> (direction === 'next' ? selectedItem.nextElementSibling : selectedItem.previousElementSibling);
        if (!nextActive && direction === 'next') {
            nextActive = <HTMLLIElement> list.firstChild;
        }
        else if (!nextActive) {
            nextActive = <HTMLLIElement> list.lastChild;  
        }
        newSelectedItem = nextActive;
    }
    newSelectedItem.classList.add('custom-suggestions--active');
    selectedItem = newSelectedItem;

    putSelectedIntoTextArea(assertNonNull(selectedItem.textContent));
};

const updateList = function() {
    // add in ability to know which option was previously being looked at
    
    const dd = assertNonNull(dropDown);
    const suggestedList = document.createElement('ul');
    suggestedList.classList.add('custom-suggestions');
    const boxWidth = document.getElementsByClassName('chat_actions_container')[0].clientWidth - 15;
    const widthStyle = 'max-width:' + boxWidth + 'px; min-width:' + boxWidth + 'px;';

    if (suggestions && textArea) {
        suggestions.forEach((suggestion) => {
            const entryItem = document.createElement('li');
            const formattedSuggestion = formatSuggestionText(suggestion);
            entryItem.innerHTML = formattedSuggestion;
            entryItem.setAttribute('style', widthStyle);
            suggestedList.appendChild(entryItem);
        });
        if (dd.firstChild) {
            dd.replaceChild(suggestedList, assertNonNull(dd.firstChild));
        }
        else {
            dd.appendChild(suggestedList);
        }
        selectedItem = null;
        if (currentText && currentText.split(/\r\n|\r|\n/).pop()) {
            console.log('toggling item');
            console.log(currentText);
            toggleItem();
        }
    }
};

const selectItem = function(suggestion: string) {
    if (textArea) {
        const currentValue = textArea.value.split(/\r\n|\r|\n/).slice(0, -1);
        currentValue.push(suggestion);
        textArea.value = currentValue.join('\r\n') + '\r\n';
    }
};

const setDropDownPosition = function(text: string) {
    const dd = assertNonNull(dropDown);
    const ta = assertNonNull(textArea);
    const { x, y } = cumulativeOffset(ta);
    const left = x + 1
    const nLines = text.split(/\r\n|\r|\n/).length;
    const top = y + 28 + ((Math.min(nLines, 8) - 1) * 16);
    
    dd.setAttribute('style', `left: ${left}px; top: ${top}px`);
};

const toggleDropdown = function(ke?: Event) {
    const clickItem = function(e: Event) {
        const ta = assertNonNull(textArea);
        e.preventDefault();
        const target = assertNonNull(e.target) as HTMLElement;
        if (target.tagName === 'LI') {
            ta.focus();
            toggleDropdown(ke);
            selectItem(assertNonNull(target.textContent));
        }
    }

    const processClick = function(evt: Event) {
        if (ke !== evt && ke && evt.target !== ke.target) {
            if (dropDownShowing) {
                toggleDropdown();
            }
            else {
                document.removeEventListener('click', processClick);
            }
        }
    }

    dropDownShowing = !dropDownShowing;

    if (dropDownShowing && !dropDown) {
        dropDown = createDropdown();
        document.body.appendChild(dropDown);
        dropDown.addEventListener('click', clickItem);
        document.addEventListener('click', processClick);
    }
    else if (dropDown) {
        dropDown.removeEventListener('click', clickItem);
        document.body.removeChild(dropDown);
        document.removeEventListener('click', processClick);
        dropDown = null;
        selectedItem = null;
    }

    if (dropDownShowing) {
        setDropDownPosition(assertNonNull(textArea).value);
    }
};

const resetSuggestions = function() {
    suggestions = [];
    if (currentText) {
        const lastLine = assertNonNull(currentText.split(/\r\n|\r|\n/).slice(-1).pop());
        if (lastLine.length > 0) {
            suggestions.push(lastLine);
        }
    }
}

const uuidv4 = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
}

let currentPartial = '';
const getNewSuggestions = function(messageType: string, customerName: string, text: string) {
    currentKey = uuidv4();

    let payload = null;
    if (messageType == 'partial') {
        currentPartial = assertNonNull(text.split('\n').slice(-1).pop());
        payload = {
            key: currentKey,
            user_name: customerName,
            partial: text
        }
    }
    else if (messageType == 'user') {
        payload = {
            key: currentKey,
            user_name: customerName,
            user_utterance: text
        }
    }
    else if (messageType = 'agent') {
        payload = {
            key: currentKey,
            user_name: customerName,
            agent_utterance: text
        }
    }
    else {
        console.log('WARNING: Unkown message type attempted to be sent to the server');
        return;
    }
    if (socket && socket.readyState == WebSocket.OPEN && payload) {
        resetSuggestions();
        socket.send(JSON.stringify(payload));

        // // wait for the suggestions to come in from the web socket
        // // have a maximum number of time to wait
        // for (let i = 0; i < 1000 && suggestions == null; i += 1) {
        //     // waiting for the ws server to send back the suggestions
        //     await sleep(10);
        //     if (i == 1000 - 1) {
        //         console.log('WARNING: took too long to get the next set of suggestions');
        //     }
        // }
    }
};

const zendeskSuppressed = function(text: string) {
    if (text.length > 0 && text[0] == '/') {
        return false;
    }
    return true;
};

const handleZendeskKeyboardInput = function(e: KeyboardEvent) {
    const ta = assertNonNull(textArea);
    const text = ta.value;
    if (e.which == TAB) {
        e.preventDefault();
    }
    else if ((e.which == UP_ARROW || e.which == DOWN_ARROW) && zendeskSuppressed(text)) {
        // if (ta.selectionEnd == text.length) {
            e.preventDefault();
        // }
    }
};

let shiftPressed = false;
const setShiftPressed = function() {
    shiftPressed = true;
}
const unsetShiftPressed = function() {
    shiftPressed = false;
}
const respondToKeyboard = function(e: KeyboardEvent) {
    // which is the keyboard value being processed
    const { which } = e;
    enterPressed = which == ENTER && shiftPressed;
    // const sideArrowPressed = which == LEFT_ARROW || which == RIGHT_ARROW
    
    switch (which) {
        case UP_ARROW:
        case DOWN_ARROW:
            if (assertNonNull(currentText).split(/\r\n|\r|\n/).slice(-1).pop() == '') {
                noTextAndSelection = true;
            }
            toggleItem(which === UP_ARROW ? 'previous' : 'next');
            break;
        case SHIFT:
            break;
        default:
            noTextAndSelection = false;
            if (!enterPressed) {
                updateList();
            }
    }
    enterPressed = false;
};

const showDropDown = async function(e: KeyboardEvent) {
    const ta = assertNonNull(textArea);
    let text = ta.value;
    let which = e.which;

    if (zendeskSuppressed(text)) {
        const zendeskShortcutsArr = document.getElementsByClassName('jx_ui_Widget textfieldlist_list_container');
        if (zendeskShortcutsArr.length > 0) {
            zendeskShortcuts = <HTMLElement> zendeskShortcutsArr[0];
        }
        else {
            zendeskShortcuts = null;
        }

        if (![UP_ARROW, DOWN_ARROW, SHIFT].includes(which) && currentCustomer) {
            currentText = text;
            console.log('currentText', currentText);
            const shouldSend = [
                '\r\n',
                '\r',
                '\n',
                ' ',
            ].some((elm) => text.endsWith(elm));
            suggestions[0] = assertNonNull(currentText.split('\n').slice(-1).pop());
            if (shouldSend) {
                console.log('sending partial');
                getNewSuggestions('partial', currentCustomer, text);
            }
        }
        if (suggestions) {
            if (which == ENTER && !shiftPressed) {
                toggleDropdown(e);
            }
            
            if (ta.selectionEnd != ta.value.length) {
                if (dropDownShowing) {
                    toggleDropdown();
                }
                return;
            }
            let numSuggestions = suggestions.length;
            const offButShouldBeOn = !dropDownShowing && numSuggestions > 0;
            const onButShouldBeOff = dropDownShowing && numSuggestions == 0;

            if (offButShouldBeOn && onButShouldBeOff) {
                console.log('ASSERTION ERROR: offButShouldBeOn and onButShouldBeOff should not both be true');
            }

            const toggle = offButShouldBeOn || onButShouldBeOff;

            if (toggle) {
                toggleDropdown(e);
            }
            if (numSuggestions > 0) {
                setDropDownPosition(ta.value);
                respondToKeyboard(e);
            }
        }
        else if (dropDownShowing) {
            toggleDropdown(e);
        }
    }
    else {
        zendeskShortcuts = null;
        if (dropDownShowing) {
            toggleDropdown(e);
        }
    }
}

const getChatLog = function(): any[] {
    const chatDivs = Array.from(document.getElementsByClassName('chat_log_line'))
                .filter((div) => {
                    const divClasses = div.classList;
                    const containsSystem = divClasses.contains('system');
                    const containsUnverified = divClasses.contains('unverified');
                    return !(containsSystem || containsUnverified);
                });
    
    let previousName = '';
    const chatLog: any[] = [];

    chatDivs.forEach((chatDiv) => {
        // get the name if it is there and update the previous name
        const headerContainer = Array.from(chatDiv.getElementsByClassName('header_container')).pop();
        if (headerContainer) {
            const nameDiv = Array.from(headerContainer.getElementsByClassName('name')).pop();
            if (nameDiv) {
                const name = nameDiv.textContent
                if (name && name != '') {
                    previousName = name;
                }
            }
        }

        const messageContainer = Array.from(chatDiv.getElementsByClassName('message_container')).pop();
        if (messageContainer) {
            const messageDiv = Array.from(messageContainer.getElementsByClassName('message'))
                                .filter((div) => !div.classList.contains('translated'))
                                .pop();
            
            if (messageDiv) {
                chatLog.push({
                    name: previousName,
                    message: messageDiv.textContent
                });
            }
        }
    });

    return chatLog;
}

const manageNewUtterances = async function(newUtterances: any[], customer: string) {
    // the number of new of new utterances should not typically be more than one
    if (newUtterances.length == 0) {
        console.log('WARNING: length of the new utterances should be greater than 0');
        return;
    }
    if (newUtterances.length > 1) {
        console.log('WARNING: length of the new utterances should not typically be greater than 1. ' + 
        'This means that the chat log updated faster than we are monitoring it. ' + 
        'Also possible at beginning of chat.');
    }
    // send off the new utterances
    newUtterances.forEach((chatMessage) => {
        const name: string = chatMessage.name;
        const message: string = chatMessage.message;

        const messageType = name == agentName ? 'agent' : 'user';

        getNewSuggestions(messageType, customer, message);
        currentPartial = '';
    });
    if (currentCustomer && newUtterances.pop().name == currentCustomer && suggestions && suggestions.length > 0) {
        if (dropDownShowing) {
            toggleDropdown();
            toggleDropdown();
        }
        else {
            toggleDropdown();
        }
        updateList();
    }
}

const monitorChatLog = function() {
    if (currentCustomer) {
        const currentChatLog = customerChats.get(currentCustomer);
        if (currentChatLog) {
            const newChatLog = getChatLog();
            const nNewChats = newChatLog.length - currentChatLog.length;
            if (nNewChats > 0) {
                customerChats.set(currentCustomer, newChatLog);
                manageNewUtterances(newChatLog.slice(-1 * nNewChats), currentCustomer);
            }
        }
    }
}

const getCustomerName = function(): string | null {
    const customerNameElement = Array.from(document.getElementsByClassName('h3 visitor_name')).pop();
    if (customerNameElement && customerNameElement.textContent) {
        return customerNameElement.textContent;
    }
    console.log('ASSERTION ERROR: The customer name should be available when this is called');
    return null;
}

const clearTextAreaAndDropdown = function() {
    if (textArea) {
        textArea.removeEventListener('keydown', handleZendeskKeyboardInput);
        textArea.removeEventListener('keypress', handleZendeskKeyboardInput);
        textArea.removeEventListener('keyup', handleZendeskKeyboardInput)
        textArea.removeEventListener('keyup', showDropDown);
        textArea.removeEventListener('keydown', setShiftPressed);
        textArea.removeEventListener('keyup', unsetShiftPressed);
    }
    if (dropDownShowing) {
        toggleDropdown();
    }
    textArea = null;
    // suggestions = null;
    currentCustomer = null;
}

setInterval(function () {
    const textAreaArr = Array.from(
        document.getElementsByClassName('meshim_dashboard_components_chatPanel_ChatTextAreaList chat_input'));

    const chatWindow = Array.from(
        document.getElementsByClassName('meshim_dashboard_components_ChatPanel chat_panel')).pop();

    if(chatWindow) {
        if (chatWindow.classList.contains('active')) {
            if (textAreaArr.length > 0) {
                let cancelAndEndButtonsOff = false;
                const cancelDiv = Array.from(document.getElementsByClassName('jx_ui_Widget container'))
                    .filter((div) => div.innerHTML.includes('EndChat')).pop();
                if (cancelDiv) {
                    const cancelStyle = cancelDiv.getAttribute('style');
                    if (cancelStyle) {
                        cancelAndEndButtonsOff = cancelStyle.includes('none');
                    }
                }
                else {
                    cancelAndEndButtonsOff = true;
                }
                if (!textArea) {
                    if (cancelAndEndButtonsOff) {
                        textArea = <HTMLTextAreaElement> textAreaArr[0];
                        textArea.addEventListener('keydown', handleZendeskKeyboardInput);
                        textArea.addEventListener('keypress', handleZendeskKeyboardInput);
                        textArea.addEventListener('keyup', handleZendeskKeyboardInput);
                        textArea.addEventListener('keyup', showDropDown);
                        textArea.addEventListener('keydown', setShiftPressed);
                        textArea.addEventListener('keyup', unsetShiftPressed);
            
                        currentCustomer = getCustomerName();
                        if (currentCustomer && !customerChats.has(currentCustomer)) {
                            customerChats.set(currentCustomer, []);
                        }
                    }
                }
                else if (!cancelAndEndButtonsOff) {
                    clearTextAreaAndDropdown();
                }
            }
        }
        else {
            clearTextAreaAndDropdown();
        }
    }
}, 200);

setInterval(function() {
    if (zendeskShortcuts) {
        zendeskShortcuts.setAttribute('style', 'display: none');
    }
}, 1);

setInterval(monitorChatLog, 50);

window.onresize = function() {
    if (dropDownShowing) {
        setDropDownPosition(assertNonNull(textArea).value);
        updateList();
    }
};

})(); // end of wrapping entire script in async
