/**
 * background script required in order to monitor network traffic which is
 * what we will use to identify the agent that is using the extension.
 */

console.log('Background Script Running');


const agentIdRequestListener = function(details: any) {
    if (details.url.includes('identitytoolkit') && details.url.includes('getAccountInfo') && details.method == 'POST') {
        if (!details.requestBody) {
            console.log('WARNING: Request body not in the details');
        }
        else {
            // get the information from the request to recreate it
            const decoder = new TextDecoder();
            const requestBody = {
                method: 'POST',
                body: decoder.decode(details.requestBody.raw[0].bytes),
                headers: {
                    'Content-Type': 'application/json'
                }
            };

            // stop listening to the requests after we get the request we need
            chrome.webRequest.onBeforeRequest.removeListener(agentIdRequestListener);
            
            // re-submit the request in order to get the id of this agent
            fetch(details.url, requestBody)
            .then((response) => {
                return response.json();
            })
            // send the id to the content script
            .then((payload) => {
                chrome.tabs.query({url: "https://zendesk-demo.answeriq.com/chat/agent/*"}, function(tabs) {
                    if (tabs[0].id) {
                        chrome.tabs.sendMessage(tabs[0].id, {agentId: payload.users[0].localId});
                    }
                });
            });
            chrome.webRequest.onBeforeRequest.addListener(
                agentIdRequestListener, 
                {urls: ["<all_urls>"]}, 
                ['requestBody']
            );
        }
    }
};

chrome.webRequest.onBeforeRequest.addListener(agentIdRequestListener, {urls: ["<all_urls>"]}, ['requestBody']);
